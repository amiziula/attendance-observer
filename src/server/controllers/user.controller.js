import User from '@/server/models/user.model';
import sanitizeHtml from 'sanitize-html';

/**
 * Search entities
 *
 * @param req
 * @param res
 * @returns void
 */
export function search(req, res) {

  const query = User.find({
    // TODO: filters
  });

  query.sort('-createdAt').exec((err, items) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ items });
  });
}

/**
 * View entity
 *
 * @param req
 * @param res
 * @returns void
 */
export function view({ params }, res) {
  const { id } = params;
  User.findOne({ _id: id })
  .populate('stats')
  .exec((err, data) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ data });
  });
}

/**
 * Save entity
 *
 * @param req
 * @param res
 * @returns void
 */
export function store({ body }, res) {

  // TODO: validators

  if (!body.firstName ||
    !body.lastName ||
    !body.email ||
    !body.password) {
    res.status(403).end();
  }

  const user = new User(body);

  user.firstName = sanitizeHtml(body.firstName);
  user.lastName = sanitizeHtml(body.lastName);
  user.email = sanitizeHtml(body.email);
  user.password = body.password;

  user.save((err, status) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ status });
  });
}

/**
 * Delete entity
 *
 * @param req
 * @param res
 * @returns void
 */
export function remove(req, res) {
  const { id } = req.params;
  User.findOne({ _id: id }).exec((err, entity) => {
    if (err) {
      res.status(500).send(err);
    }
    entity.remove(() => {
      res.status(200).end();
    });
  });
}