import User, {ROLE_USER} from '@/server/models/user.model';
import {
  encryptPassword,
  comparePassword,
  validateLogin,
  validateSignup,
} from '@/server/services/user.service';

import jwt from 'jsonwebtoken';
import config from '../config';

export async function me(req, res) {
  const user = await User.findById(req.user.id);
  // TODO: refactory
  if (user) {
    user.password = undefined;
  }
  return res.status(200).json(user);
}

/**
 * @param {Object} req
 * @param {Object} res
 * @returns {Promise<void>}
 */
export async function logout(req, res) {
  // TODO: revoke token ?
  // res.redirect('/');
  res.status(200).send({auth: false, token: null});
}

/**
 * @param {Object} req
 * @param {Object} res
 * @returns {Promise<void>}
 */
export async function login(req, res) {
  try {
    const {value, error} = validateLogin(req.body);
    if (error) {
      return res.status(400).json(error);
    }
    const user = await User.findOne({email: value.email});
    if (user && comparePassword(value.password, user.password)) {
      const token = jwt.sign({id: user._id}, config.jwt.secret,
        {expiresIn: config.jwt.expiresIn});
      return res.json({
        user,
        token,
        expiresIn: config.jwt.expiresIn,
      });
    } else {
      return res.status(400).json({message: 'Wrong email or password!'});
    }
  } catch (err) {
    return res.status(500).send(err);
  }
}

/**
 * @param {Object} req
 * @param {Object} res
 * @returns {Promise<void>}
 */
export async function register(req, res) {
  try {
    const {value, error} = validateSignup(req.body);
    if (error) {
      return res.status(400).json(error);
    }
    const encryptedPass = encryptPassword(value.password);
    const user = await User.create({
      email: value.email,
      firstName: value.firstName,
      lastName: value.lastName,
      password: encryptedPass,
      role: value.role || ROLE_USER,
    });
    return res.json({success: true});
  } catch (err) {
    return res.status(500).send(err);
  }
}

export async function forgotPassword(req, res) {
  // TODO: ...
  res.status(401).json({
    message: 'Not implemented',
  });
}

export async function resetPassword(req, res) {
  // TODO: ...
  res.status(401).json({
    message: 'Not implemented',
  });
}