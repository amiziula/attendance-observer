import Stat from '@/server/models/stat.model';

/**
 * Search entities
 *
 * @param req
 * @param res
 * @returns void
 */
export function search(req, res) {
  const query = Stat.find({
      // TODO: filters
    }).
    populate({path: 'user', select: ['firstName', 'lastNam', 'email', 'gender']}).
    populate({path: 'subject', select: ['name']})
  ;
  query.exec((err, items) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({items});
  });
}
