import Subject from '@/server/models/subject.model';
import sanitizeHtml from 'sanitize-html';

/**
 * Search entities
 *
 * @param req
 * @param res
 * @returns void
 */
export function search(req, res) {
  const query = Subject.find({
    // TODO: filters
  });
  query.sort('-createdAt').exec((err, items) => {
    if (err) {
      res.status(500).send(err);
    }
    // TODO: paginate
    res.json({ items });
  });
}

/**
 * View entity
 *
 * @param req
 * @param res
 * @returns void
 */
export function view(req, res) {
  const { id } = req.params;
  Subject.findOne({ _id: id }).exec((err, data) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ data });
  });
}

/**
 * Save entity
 *
 * @param req
 * @param res
 * @returns void
 */
export function store({ body }, res) {
  if (!body.name) {
    res.status(403).end();
  }

  const subject = new Subject(body);
  subject.name = sanitizeHtml(body.name);
  subject.description = sanitizeHtml(body.description);

  subject.save((err, status) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ status });
  });
}

/**
 * Delete entity
 *
 * @param req
 * @param res
 * @returns void
 */
export function remove(req, res) {
  const { id } = req.params;
  Subject.findOne({ _id: id }).exec((err, entity) => {
    if (err) {
      res.status(500).send(err);
    }
    entity.remove(() => {
      res.status(200).end();
    });
  });
}