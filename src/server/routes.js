import { Router } from 'express';
import jwt from 'express-jwt';
import config from "./config";
import * as subjectController from '@/server/controllers/subject.controller';
import * as userController from '@/server/controllers/user.controller';
import * as authController from '@/server/controllers/auth.controller';
import * as statsController from '@/server/controllers/stats.controller';
import {seedRoute} from '@/server/services/seed.service';

const router = new Router();

const authMiddleware = jwt({secret: config.jwt.secret});

// SEEDER

// if (isDevMode) {
router.route('/seed').get(seedRoute);
// }

// AUTH

router.route('/me').get(authMiddleware, authController.me);
router.route('/auth/login').post(authController.login);
router.route('/auth/logout').post(authMiddleware, authController.logout);
router.route('/auth/signup').post(authController.register);
router.route('/auth/forgot-password').post(authController.forgotPassword);
router.route('/auth/reset-password').post(authController.resetPassword);

// STATS

router.route('/stats').get(authMiddleware, statsController.search);

// USERS

router.route('/users').get(authMiddleware, userController.search);
router.route('/users/:id').get(authMiddleware, userController.view);
router.route('/users').post(authMiddleware, userController.store);
router.route('/users/:id').delete(authMiddleware, userController.remove);

// SUBJECTS

router.route('/subjects').get(authMiddleware, subjectController.search);
router.route('/subjects/:id').get(authMiddleware, subjectController.view);
router.route('/subjects').post(authMiddleware, subjectController.store);
router.route('/subjects/:id').delete(authMiddleware, subjectController.remove);

export default router;