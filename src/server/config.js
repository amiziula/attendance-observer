const config = {
  mongoURL: process.env.MONGO_URL || 'mongodb://mongo:27017/observer',
  port: process.env.PORT || 8080,
  jwt: {
    secret: '1q2w3e4r',
    expiresIn: 129600
  },
};

export default config;