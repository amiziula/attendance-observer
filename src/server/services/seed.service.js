import User from '@/server/models/user.model';
import Group from '@/server/models/group.model';
import Subject from '@/server/models/subject.model';
import Stat, {TYPE_CHECKIN} from '@/server/models/stat.model';
import faker from 'faker';
import sample from 'lodash/sample';

export async function seedRoute(req, res) {
  console.log('Seed users...');
  await seedUsers(10);

  console.log('Seed Groups...');
  await seedGroups();

  console.log('Seed Subjects...');
  await seedSubjects();

  console.log('Seed Stats...');
  await seedStats(1000);

  console.log('ok...');

  return res.send('ok');
}

/**
 * Seed Users
 */
export async function seedUsers(total = 100) {

  console.log('Clear users...');
  await User.remove({});

  const items = [
    // presets
    {
      firstName: 'Super',
      lastName: 'Man',
      email: 'admin@ad.min',
      password: 'admin',
    },
  ];

  for (let i = 0; i < total; i++) {
    items.push(new User({
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      locale: faker.random.locale(),
      gender: faker.random.number({max: 2}),
      email: faker.internet.email(),
      phone: faker.phone.phoneNumberFormat(2),
      password: 'secret',
    }));
  }

  await User.create(items, (error) => {
    if (error) {
      throw error;
    }
  });
}

/**
 * Seed Groups
 */
export async function seedGroups() {
  console.log('Clear Groups...');
  await Group.remove({});

  const items = [
    {
      name: 'B52',
      description: 'test',
    },
  ];

  await Group.create(items, (error) => {
    if (error) {
      throw error;
    }
  });
}

/**
 * Seed Subjects
 */
export async function seedSubjects() {

  console.log('Clear Subjects...');
  await Subject.remove({});

  const items = [
    {
      name: 'Math',
      requiredHours: 40,
    },
    {
      name: 'Biology',
      requiredHours: 20,
    },
    {
      name: 'Chemistry',
      requiredHours: 55,
    },
      {
      name: 'Programing',
      requiredHours: 20,
    },
      {
      name: 'Religion',
      requiredHours: 15,
    },
      {
      name: 'JavaScript',
      requiredHours: 30,
    },
  ];

  await Subject.create(items, (error) => {
    if (error) {
      throw error;
    }
  });
}

/**
 * Seed Stats
 */
export async function seedStats(total = 100) {

  console.log('Clear Stats...');
  await Stat.remove({});

  const items = [];

  const randomUsers = await User.aggregate([
    {$sample: {size: 100}},
  ]);

  const randomSubjects = await Subject.aggregate([
    {$sample: {size: 100}},
  ]);

  for (let i = 0; i < total; i++) {
    const user = sample(randomUsers);
    const subject = sample(randomSubjects);
    items.push(new Stat({
      type: TYPE_CHECKIN,
      user: user._id,
      subject: subject._id,
      createdAt: faker.date.past(),
    }));
  }

  await Stat.create(items, (error) => {
    if (error) {
      throw error;
    }
  });
}