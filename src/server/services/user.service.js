import Joi from 'joi';
import bcrypt from 'bcryptjs';

export function encryptPassword(palinText) {
  const salt = bcrypt.genSaltSync(10);
  return bcrypt.hashSync(palinText, salt);
}

export function comparePassword(plainText, encrypedPassword) {
  return bcrypt.compareSync(plainText, encrypedPassword);
}

export function validateSignup(body) {
  const schema = Joi.object().keys({
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    email: Joi.string().email().required(),
    password: Joi.string().required(),
    role: Joi.number().integer(),
  });
  const {value, error} = Joi.validate(body, schema);
  if (error && error.details) {
    return {error};
  }
  return {value};
}

export function validateLogin(body) {
  const schema = Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  });
  const {value, error} = Joi.validate(body, schema);
  if (error && error.details) {
    return {error};
  }
  return {value};
}
