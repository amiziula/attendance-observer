import Express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import compression from 'compression';
import jwt from 'express-jwt';
import cors from 'cors';

import config from './config';
import routes from './routes';

// Set native promises as mongoose promise
mongoose.Promise = global.Promise;

// MongoDB Connection
// if (process.env.NODE_ENV !== 'test') {
mongoose.connect(config.mongoURL, {
  useNewUrlParser: true,
}, (error) => {
  if (error) {
    console.error('Please make sure Mongodb is installed and running!'); // eslint-disable-line no-console
    throw error;
  }
});
// }

const app = Express();

// Set Development modes checks
const isDevMode = process.env.NODE_ENV === 'development' || false;
const isProdMode = process.env.NODE_ENV === 'production' || false;

// Run Webpack dev server in development mode
// if (isDevMode) {
//   // Webpack Requirements
//   // eslint-disable-next-line global-require
//   const webpack = require('webpack');
//   // eslint-disable-next-line global-require
//   const config = require('../webpack.config.dev');
//   // eslint-disable-next-line global-require
//   const webpackDevMiddleware = require('webpack-dev-middleware');
//   // eslint-disable-next-line global-require
//   const webpackHotMiddleware = require('webpack-hot-middleware');
//   const compiler = webpack(config);
//   app.use(webpackDevMiddleware(compiler, {
//     noInfo: true,
//     publicPath: config.output.publicPath,
//     watchOptions: {
//       poll: 1000,
//     },
//   }));
//   app.use(webpackHotMiddleware(compiler));
// }

if (isProdMode) {
  app.use(cors());
  app.use(compression());
}

app.use(bodyParser.json());

// app.use(Express.static(path.resolve(__dirname, '../dist/client')));

// ------------------------------------------------
// Routes
// ------------------------------------------------

app.use('/api', routes);

app.use(
  jwt({secret: config.jwt.secret}).unless({
    path: [
      '/api/auth/signup',
      '/api/auth/login',
      '/api/auth/forgot-password',
      '/api/auth/reset-password',
    ],
  }),
);

app.use((err, req, res, next) => {
  if (err.name === 'UnauthorizedError') {
    res.status(401).send('Missing authentication credentials.');
  }
});

app.listen(config.port, error => {
  if (!error) {
    console.log(`App is running on port: ${config.port}!`); // eslint-disable-line
  }
  // fs.readdirSync(path.join(__dirname, 'routes')).map(file => {
  //   require('./routes/' + file)(app);
  // });
});

module.exports = app;