import mongoose from 'mongoose';
// import { autoIncrement } from 'mongoose-plugin-autoinc';
// import bcrypt from 'bcryptjs';

import {encryptPassword} from '@/server/services/user.service';

const Schema = mongoose.Schema;

// TODO: move to constants

export const ROLE_USER = 0;
export const ROLE_MANAGER = 1;
export const ROLE_ADMIN = 999;

export const STATUS_BLOCKED = -1;
export const STATUS_ACTIVE = 1;
export const STATUS_INACTIVE = 0;

export const GENDER_MALE = 1;
export const GENDER_FEMALE = 2;

const UserSchema = new Schema({

  email: {
    type: String,
    trim: true,
    unique: true,
    index: true,
    lowercase: true,
    required: true,
  },

  firstName: {
    type: String,
    trim: true,
    required: true,
  },

  lastName: {
    type: String,
    trim: true,
    required: true,
  },

  password: {
    type: String,
    default: '',
  },

  gender: {
    type: Number,
    default: 0,
  },

  role: {
    type: String,
    default: ROLE_USER,
  },

  group: {
    type: String,
    default: '',
  },

  phone: {
    type: String,
    default: '',
  },

  // Date Of Birth
  dob: {
    type: Date,
    default: null,
  },

  resetPasswordToken: String,
  resetPasswordExpires: Date,

  verified: {
    type: Boolean,
    default: false,
  },

  verifyToken: {
    type: String,
  },

  status: {
    type: Number,
    default: STATUS_ACTIVE,
  },

  studyDate: {
    type: Date,
    default: null,
  },

  createdAt: {
    type: Date,
    default: Date.now,
  },

  stats: [{type: Schema.Types.ObjectId, ref: 'Stat'}],

});

/**
 * Password hashing
 */
UserSchema.pre('save', function(next) {
  if (this.isModified('password')) {
    this.password = encryptPassword(this.password);
  }
  return next();
});

/**
 * Pick is only some fields of object
 *
 * http://mongoosejs.com/docs/api.html#document_Document-toObject
 *
 */
UserSchema.methods.pick = function(props, model) {
  return _.pick(model || this.toJSON(), props || [
    'email',
    'firstName',
    'lastName',
    'role',
    'group',
    'dob',
    'phone',
    'status',
  ]);
};

// UserSchema.plugin(autoIncrement, 'User');

export default mongoose.model('User', UserSchema);