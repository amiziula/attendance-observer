import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const SubjectSchema = new Schema({
  name: {type: String, required: true},
  description: {type: String},
  requiredHours: {type: Number, default: 0},
  duration: {type: Number, default: 0},
  createdAt: {type: Date, default: Date.now},
});

export default mongoose.model('Subject', SubjectSchema);