import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const GroupSchema = new Schema({
  name: {type: String, required: true},
  description: {type: String},
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  createdAt: {type: Date, default: Date.now},
});

export default mongoose.model('Group', GroupSchema);