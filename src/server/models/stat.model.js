import mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const TYPE_CHECKIN = 'checkin';
export const TYPE_CHECKOUT = 'checkout';
// TODO: more types ...

const StatSchema = new Schema({
  type: {type: String, required: true},
  description: {type: String},
  subject: {type: Schema.Types.ObjectId, ref: 'Subject'},
  user: {type: Schema.Types.ObjectId, ref: 'User'},
  // meta: {type: Object},
  createdAt: {type: Date, default: Date.now},
});

export default mongoose.model('Stat', StatSchema);