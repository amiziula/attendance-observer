import React from 'react';
import Frisbee from 'frisbee';
import store from '@/client/store';
import {END_SESSION} from '@/client/store/reducers/auth.reducer';

const api = new Frisbee({
  baseURI: '/api',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
});

// TODO: refactory
store.subscribe(function() {
  const {auth} = store.getState();
  if (auth.token) {
    api.jwt(auth.token);
  }
});

api.interceptor.register({
  response(resp) {
    const {status} = resp;
    if (status === 401) {
      // Force logout when token expired
      store.dispatch({type: END_SESSION});
      // window.location.href = '#/login';
    }
    return resp;
  },
});

// Api Endpoints
// ------------------------------------------

api.endpoints = {
  me() {
    return api.get('me');
  },
  getUsers() {
    return api.get('users');
  },
  getStats() {
    return api.get('stats');
  },
  getSubjects() {
    return api.get('subjects');
  }
};

export default api;