export const SET_TOKEN = 'SET_TOKEN';
export const END_SESSION = 'END_SESSION';
export const LOGIN_PENDING = 'LOGIN_PENDING';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_ERROR = 'LOGIN_ERROR';

const STORAGE_TOKEN_KEY = 'authToken';

const initialState = {
  token: localStorage.getItem(STORAGE_TOKEN_KEY) || '',
  loading: false,
  user: {},
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_TOKEN:
      localStorage.setItem(STORAGE_TOKEN_KEY, action.payload);
      return {
        ...state,
        token: action.payload,
      };
    case END_SESSION:
      localStorage.removeItem(STORAGE_TOKEN_KEY);
      return {
        ...state,
        token: null,
        user: {},
      };
    case LOGIN_PENDING:
      return {
        ...state,
        loading: true,
      };
    case LOGIN_SUCCESS:
      const {token, user} = action.payload;
      localStorage.setItem(STORAGE_TOKEN_KEY, token);
      return {
        ...state,
        loading: false,
        token,
        user,
      };
    case LOGIN_ERROR:
      return {
        ...state,
        loading: false,
        token: null,
      };
    default:
      return state;
  }
}