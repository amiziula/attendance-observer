export const FETCH_FINISH = 'FETCH_FINISH';
export const FETCH_START = 'FETCH_START';

const initialState = {
  loading: false,
};

export default function loading(state = initialState, action) {
  switch (action.type) {
    case FETCH_START:
      return {
        ...state,
        loading: true,
      };
    case FETCH_FINISH:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
}