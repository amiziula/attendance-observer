import {
  LOGIN_PENDING,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  END_SESSION,
} from '../reducers/auth.reducer';
import api from '@/client/services/api';

export function logout() {
  return async dispatch => {
    dispatch({type: END_SESSION});
    return Promise.resolve(true);
  };
}

export function login(body) {
  return async dispatch => {
    try {
      dispatch({type: LOGIN_PENDING});
      const resp = await api.post('auth/login', {body});

      if (resp.ok) {
        dispatch({
          type: LOGIN_SUCCESS, payload: resp.body,
        });
      } else {
        dispatch({
          type: LOGIN_ERROR, payload: resp.err,
        });
      }
      return resp;
    } catch (error) {
      dispatch({type: LOGIN_ERROR});
      return error;
    }
  };
}