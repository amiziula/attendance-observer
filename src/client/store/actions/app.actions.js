import {FETCH_START, FETCH_FINISH} from '../reducers/app.reducer';
// import api from '@/client/services/api';

export function fetchFinish() {
  return dispatch => {
    dispatch({
      type: FETCH_FINISH,
    });
  };
}

export function fetchStart() {
  return dispatch => {
    dispatch({
      type: FETCH_START,
    });
  };
}