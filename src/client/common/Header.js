import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Navigation from './Navigation';
import {Logo} from '../assets';

export default class Header extends Component {
  render() {
    return (
      <div className="header">
        <Link className="logo" to="/">
          <img src={Logo} alt=""/>
        </Link>
        <Navigation/>
      </div>
    );
  }
}