import React, {Component} from 'react';
import {Link, NavLink} from 'react-router-dom';
import {Icon, Modal, Button, Header} from 'semantic-ui-react';

class ModalNav extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalOpen: false,
    };

    this.handleOpen = () => this.setState({modalOpen: true});
    this.handleClose = () => this.setState({modalOpen: false});
  }

  render() {
    return (
      <React.Fragment>
        <Modal centered={false}
               basic
               className="burger-nav"
               name="bars"
               open={this.state.modalOpen}
               trigger={<Icon name="bars" className="burger-ico"
                              onClick={this.handleOpen}/>
               }>
          <Modal.Content>
            <Menu onClick={this.handleClose.bind(this)}/>
            <ul className='main-nav'>
              <li>
                <span onClick={this.handleClose}>Close</span>
              </li>
            </ul>
          </Modal.Content>
        </Modal>
      </React.Fragment>
    );
  }
}

class Menu extends Component {

  static defaultProps = {
    onClick: () => {
    },
  };

  render() {
    return (
      <ul className='main-nav'>
      <li>
          <Link onClick={this.props.onClick} to='/'><Icon name="qrcode" />Home</Link>
      </li>
      <li>
          <NavLink onClick={this.props.onClick} to='/statistic' activeClassName="active"><Icon name="line graph" />Statistic</NavLink>
      </li>
      <li>
          <NavLink onClick={this.props.onClick} to='/subjects' activeClassName="active"><Icon name="book" />Subjects</NavLink>
      </li>
      <li>
          <NavLink onClick={this.props.onClick} to='/users' activeClassName="active"><Icon name="users" /> Users</NavLink>
      </li>
      <li>
          <NavLink onClick={this.props.onClick} to='/user/' activeClassName="active"><Icon name="add user" /> Add User</NavLink>
      </li>
      <li>
          <NavLink onClick={this.props.onClick} to='/subject/' activeClassName="active"><Icon name="add" /> Add Subject</NavLink>
      </li>
      <li>
          <NavLink onClick={this.props.onClick} to='/faq' activeClassName="active"><Icon name="info" /> FAQ</NavLink>
      </li>
        <li>
          <NavLink to='/logout'><Icon name="log out" /></NavLink>
        </li>
      </ul>);
  }
}

export default class Navigation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mobile: window.screen.width < 500,
    };
  }

  render() {
    const {mobile} = this.state;

    return (
      <React.Fragment>
        {!mobile ? <Menu/> : <ModalNav/>}
      </React.Fragment>
    );
  }
}