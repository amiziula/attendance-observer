import React, {Component} from 'react';
import Progress from 'react-progress-2';
import {connect} from 'react-redux';

class ProgressLine extends Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.state) {
      Progress.show();
    } else {
      Progress.hide();
    }
  }

  render() {
    return (
      <Progress.Component/>
    );
  }
}

function mapStateToProps(state) {
  return {
    state: state.app.loading,
  };
}

export default connect(mapStateToProps)(ProgressLine);