import React, {Component} from 'react';
import {Table, Label, Icon, Header, Dimmer, Loader} from 'semantic-ui-react';
import moment from 'moment';
import {fetchStart, fetchFinish} from '../store/actions/app.actions';
import api from '@/client/services/api';

import {connect} from 'react-redux';

class StatisticList extends Component {

  state = {
    stats: [],
  };

  async componentDidMount() {
    this.props.fetchStart();
    const resp = await api.endpoints.getStats();
    if (resp.ok) {
      this.setState({stats: resp.body.items});
    }
    this.props.fetchFinish();
  }

  renderStats() {
    const {stats} = this.state;
    const statsList = [];

    stats.forEach((stat, index) => {
      statsList.push(
        <Table.Row key={stat._id}>
          <Table.Cell><Label ribbon>{index}</Label></Table.Cell>
          <Table.Cell>{stat.user.firstName}</Table.Cell>
          <Table.Cell>{stat.subject.name}</Table.Cell>
          <Table.Cell>{moment(stat.createdAt).format('ll')}</Table.Cell>
        </Table.Row>,
      );
    });

    return statsList;
  }

  render() {
    const {stats} = this.state;

    return (
      <React.Fragment>
        <Header as='h1'>Statistic</Header>
        <Table celled selectable compact='very' color="teal">
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell width={1}></Table.HeaderCell>
              <Table.HeaderCell>User ID</Table.HeaderCell>
              <Table.HeaderCell>Subject</Table.HeaderCell>
              <Table.HeaderCell>Date of visiting</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {stats.length ? this.renderStats() : <Table.Row>
              <Table.Cell colSpan={4} textAlign="center"> No Statistic Data</Table.Cell>
            </Table.Row>}
          </Table.Body>
        </Table>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    loading: state.loading,
  };
}

const mapDispatchToProps = {
  fetchFinish,
  fetchStart,
};

export default connect(mapStateToProps, mapDispatchToProps)(StatisticList);