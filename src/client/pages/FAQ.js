import React, {Component} from 'react';
import {Segment, Divider, Header} from 'semantic-ui-react';

export default class FAQ extends Component {
  render() {
    return (
      <div>
        <Header as='h1'>FAQ</Header>
        <Segment color='yellow'>
          <b>1. What the gist?</b>
          <Divider/>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut
            labore...</p>
          <b>2. How it works?</b>
          <Divider/>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut
            labore...</p>

          <b>3. What requirements?</b>
          <Divider/>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut
            labore...</p>
        </Segment>
      </div>
    );
  }
}