import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Button, Form, Grid, Message, Segment} from 'semantic-ui-react';
import * as authActions from '@/client/store/actions/auth.actions';
import {Logo} from '../assets';

class LoginForm extends Component {

  state = {
    data: {
      email: null,
      password: null,
    },
    errors: {},
  };

  handleSubmit = async (e) => {
    e.preventDefault();

    const {login} = this.props;

    await this.setStateAsync({loading: true});

    const resp = await login(this.state.data);
    const errors = {};

    if (resp.err) {
      if (resp.err.message) {
        errors.common = resp.err.message;
      }
      // multiple errors
      else if (resp.body.details) {
        resp.body.details.forEach(err => {
          errors[err.context.key] = err.message;
        });
      }
      this.setState({errors});
    } else {
      // TODO: redirect
    }

    this.setState({errors, loading: false});
  };

  handleInputChange = e => {
    this.setState({
      data: {
        ...this.state.data,
        [e.target.name]: e.target.value,
      },
    });
  };

  setStateAsync(state) {
    return new Promise((resolve) => {
      this.setState(state, resolve);
    });
  }

  render() {

    const {loading} = this.props;
    const {data, errors} = this.state;
    const errorMessage = Object.entries(errors).map(err => err[1]).join('\n');

    return (
      <div className='login-form'>
        <style>{`
      body > div,
      body > div > div,
      body > div > div > div.login-form {
        height: 100%;
      }
    `}</style>

        <Grid textAlign='center' style={{height: '100%'}}
              verticalAlign='middle'>
          <Grid.Column style={{maxWidth: 450}}>

            <h2>
              <img className="logo" src={Logo} alt=""/>
            </h2>

            {errorMessage && <Message error content={errorMessage}/>}

            <Form size='large' loading={loading} onSubmit={this.handleSubmit}
                  error={errors.length > 0}>
              <Segment stacked>
                <Form.Input
                  fluid
                  icon='user'
                  iconPosition='left'
                  placeholder='E-mail address'
                  name='email'
                  defaultValue={data.email}
                  error={!!errors.email}
                  onChange={this.handleInputChange}
                />
                <Form.Input
                  fluid
                  icon='lock'
                  iconPosition='left'
                  placeholder='Password'
                  type='password'
                  name='password'
                  defaultValue={data.password}
                  error={!!errors.password}
                  onChange={this.handleInputChange}
                />
                <Button color='teal' fluid size='large'>
                  Login
                </Button>
              </Segment>

            </Form>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

export default connect(state => {
  return {
    loading: state.auth.loading,
  };
}, authActions)(LoginForm);
