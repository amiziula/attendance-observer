import React, {Component} from 'react';
import {Form, Header, Segment, Modal} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {fetchStart, fetchFinish} from '../store/actions/app.actions';
import api from '@/client/services/api';
import { withRouter } from "react-router-dom";

class SubjectAdd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subject: {},
    };
  }

  onChangeHandler(e) {
    const {subject} = this.state;
    const newSubjectData = Object.assign(subject,
      {[e.target.name]: e.target.value});

    this.setState({
      subject: newSubjectData,
    });
  }

  onSubmit() {
    const {subject} = this.state;

    const redirect = () => {
        this.props.history.push('/subjects');
    };

    this.props.fetchStart();
    api.post('subjects/', {
        body: subject,
    }).then(function(response) {
        redirect();
    })
  }

  render() {
    const {subject} = this.state;

    return (
      <React.Fragment>
        <Header as='h1'>Add new subject</Header>
        <Segment color='green'>
          <Form onSubmit={this.onSubmit.bind(this)}>
            <Form.Field>
              <label>Subject name: </label>
              <input
                onChange={this.onChangeHandler.bind(this)}
                name='name'
                value={subject.firstName}
              />
            </Form.Field>

            <Form.Button>Submit</Form.Button>
          </Form>
        </Segment>
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = {
  fetchFinish,
  fetchStart,
};

export default withRouter(connect(null, mapDispatchToProps)(SubjectAdd));