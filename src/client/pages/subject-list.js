import React, {Component} from 'react';
import {Table, Label, Icon, Header, Dimmer, Loader} from 'semantic-ui-react';
import {fetchStart, fetchFinish} from '../store/actions/app.actions';

import {connect} from 'react-redux';
import api from '@/client/services/api';

class SubjectList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            subjects: [],
        };
    }

    async componentDidMount() {
        this.props.fetchStart();
        const resp = await api.endpoints.getSubjects();
        if (resp.ok) {
            this.setState({subjects: resp.body.items});
        }
        this.props.fetchFinish();
    }

    renderSubjectList(sub, index) {
        return (
            <Table.Row key={sub._id}>
                <Table.Cell><Label ribbon>{index}</Label></Table.Cell>
                <Table.Cell>{sub._id}</Table.Cell>
                <Table.Cell>{sub.name}</Table.Cell>
                <Table.Cell>{sub.requiredHours}</Table.Cell>
                <Table.Cell>{sub.duration}</Table.Cell>
            </Table.Row>
        );
    }

    render() {
        const {subjects} = this.state;

        return (
            <React.Fragment>
                <Header as='h1'>Subjects </Header>
                <Table celled selectable compact='very' color="teal">
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell width={1}></Table.HeaderCell>
                            <Table.HeaderCell width={1}>id</Table.HeaderCell>
                            <Table.HeaderCell>Subject</Table.HeaderCell>
                            <Table.HeaderCell>Required Hours</Table.HeaderCell>
                            <Table.HeaderCell>Duration</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {subjects.length ? subjects.map(this.renderSubjectList) :
                            <Table.Row>
                                <Table.Cell colSpan={5} textAlign="center"> No Subjects
                                    Data</Table.Cell>
                            </Table.Row>}
                    </Table.Body>
                </Table>
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        loading: state.loading,
    };
}

const mapDispatchToProps = {
    fetchFinish,
    fetchStart,
};

export default connect(mapStateToProps, mapDispatchToProps)(SubjectList);