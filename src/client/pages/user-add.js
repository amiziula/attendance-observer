import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import {Form, Header, Segment, Modal} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {fetchStart, fetchFinish} from '../store/actions/app.actions';
import { withRouter } from "react-router-dom";
import api from "../services/api";

class UserAdd extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: false,
    };

    this.id = this.props.match.params.id ? this.props.match.params.id : '';
  }

  componentDidMount() {
    if (this.id) {
      this.props.fetchStart();
      fetch('/api/users/' + this.id).then(function(response) {
        return response.json();
      }).then((data) => {
        this.setState({
          user: data.data,
        });

        this.props.fetchFinish();
      });
    }
  }

  onChangeHandler(e) {
    const {user} = this.state;
    const newUserData = !user ? {[e.target.name]: e.target.value} : Object.assign(user, {[e.target.name]: e.target.value});

    console.log(newUserData)

    this.setState({
      user: newUserData,
    });
  }

  onSubmit() {
    const {user} = this.state;

      const redirect = () => {
          this.props.history.push('/users');
      };

    this.props.fetchStart();
      api.post('/users', {
          body: user,
      }).then(function(response) {
          redirect();
      })
  }

  render() {
    const options = [
      {key: 'm', text: 'Male', value: '0'},
      {key: 'f', text: 'Female', value: '1'},
    ];
    const {user} = this.state;

    return (
      <React.Fragment>
        <Header as='h1'>{user ? 'Edit user' : 'Add new user'}</Header>
        <Segment color='green'>
          <Form onSubmit={this.onSubmit.bind(this)}>
            <Form.Field>
              <label>Name: </label>
              <input
                onChange={this.onChangeHandler.bind(this)}
                name='firstName'
                value={user.firstName}
              />
            </Form.Field>
            <Form.Field>
              <label>Second name:</label>
              <input
                onChange={this.onChangeHandler.bind(this)}
                name='lastName'
                value={user.lastName}
              />
            </Form.Field>
            <Form.Field>
              <Form.Select fluid label='Gender' defaultValue={user.gender}
                           options={options} placeholder='Select gender'/>
            </Form.Field>
            <Form.Group widths='equal'>
              <Form.Input
                onChange={this.onChangeHandler.bind(this)}
                fluid
                label='Group'
                value={user.group}
                placeholder='Group'
              />
              <Form.Input
                onChange={this.onChangeHandler.bind(this)}
                fluid
                label='Course'
                value={user.group}
                placeholder='Course'
              />
            </Form.Group>
            <Form.Field>
              <label>Phone:</label>
              <input
                name='phone'
                value={user.phone}
                onChange={this.onChangeHandler.bind(this)}
              />
            </Form.Field>
            <Form.Field>
              <label>Email:</label>
              <input
                name='email'
                value={user.email}
                onChange={this.onChangeHandler.bind(this)}
              />
            </Form.Field>
            <Form.Field>
              <Form.TextArea
                placeholder='Adress'
                name='adress'
                value={user.adress}
                onChange={this.onChangeHandler.bind(this)}
              />
            </Form.Field>
            <Form.Button>Submit</Form.Button>
          </Form>
        </Segment>
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = {
  fetchFinish,
  fetchStart,
};

export default withRouter(connect(null, mapDispatchToProps)(UserAdd));