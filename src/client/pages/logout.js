import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import * as authActions from '@/client/store/actions/auth.actions';

class LogoutPage extends Component {
  componentWillMount() {
    this.props.logout();
  }

  render() {
    return (
      <Redirect to="/"/>
    );
  }
}

export default connect(null, {
  logout: authActions.logout,
})(LogoutPage);