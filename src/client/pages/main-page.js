import React, {Component} from 'react';
import QrReader from 'react-qr-reader';
import {Container, Button, Icon, Message} from 'semantic-ui-react';

export default class MainPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showQR: false,
      delay: 300,
      result: '',
    };
  }

  triggerQr() {
    const {showQR} = this.state;

    this.setState({
      showQR: !showQR,
    });
  };

  handleError(err) {
    console.error(err);
  }

  handleScan(data) {
    if (data) {
      this.setState({
        result: data,
        showQR: false,
      });
    }
  }

  renderResult(result) {
    return (
      <Message positive>
        <Message.Header>QR was read</Message.Header>
        <p>
          Go to <b><a href={result}>{result}</a></b>.
        </p>
      </Message>
    );
  }

  render() {
    const {showQR, delay, result} = this.state;

    return (
      <Container className='center aligned' fluid>
        <Button
          onClick={this.triggerQr.bind(this)}
          color='teal' icon='qrcode'
          size='massive'
          content='Scan QR code'/>

        {result ? this.renderResult(result) : null}
        {showQR ? <QrReader
          className='qr-reader'
          delay={delay}
          onError={this.handleError.bind(this)}
          onScan={this.handleScan.bind(this)}
        /> : null}
      </Container>
    );
  }
}