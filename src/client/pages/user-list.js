import React, {Component} from 'react';
import {Table, Label, Icon, Header, Button} from 'semantic-ui-react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {
  fetchFinish,
  fetchStart,
} from '../store/actions/app.actions';
import api from '@/client/services/api';

class UserList extends Component {

  state = {
    users: [],
  };

  async componentDidMount() {
    this.props.fetchStart();
    const resp = await api.endpoints.getUsers();
    if (resp.ok) {
      this.setState({users: resp.body.items});
    }
    this.props.fetchFinish();
  }

  renderUser() {
    const {users} = this.state;
    const userList = [];

    users.forEach((user) => {
      userList.push(
        <Table.Row key={user._id}>
          <Table.Cell><Label ribbon>{user._id}</Label></Table.Cell>
          <Table.Cell>{user.firstName} {user.lastName}</Table.Cell>
          <Table.Cell>{user.group ? user.group : '-'}</Table.Cell>
          <Table.Cell>{user.studyDate ? user.studyDate : '-'}</Table.Cell>
          <Table.Cell>{user.gender === 1 ? 'Male' : 'Female'}</Table.Cell>
          <Table.Cell>{user.phone}</Table.Cell>
          <Table.Cell>{user.email}</Table.Cell>
          <Table.Cell
            positive={user.status === 1 ? true : false}
            negative={user.status !== 1 ? true : false}
            textAlign="center">
            {user.status === 1 ? <Icon color="green" name="checkmark"/> : null}
          </Table.Cell>
          <Table.Cell textAlign="center">
            <Link to={'user/' + user._id}><Button icon='setting' color='green'
                                                  size='mini'/></Link>
          </Table.Cell>
        </Table.Row>,
      );
    });

    return userList;
  }

  render() {
    const {users} = this.state;

    return (
      <React.Fragment>
        <Header as='h1'>Users</Header>
        <Table celled selectable compact='very' color="violet">
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell width={1}>id</Table.HeaderCell>
              <Table.HeaderCell>User name</Table.HeaderCell>
              <Table.HeaderCell>Groupe</Table.HeaderCell>
              <Table.HeaderCell>Course</Table.HeaderCell>
              <Table.HeaderCell>Gender</Table.HeaderCell>
              <Table.HeaderCell>Phone</Table.HeaderCell>
              <Table.HeaderCell>Mail</Table.HeaderCell>
              <Table.HeaderCell width={1}>Status</Table.HeaderCell>
              <Table.HeaderCell width={1}></Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {users.length ? this.renderUser() :
              <Table.Row>
                <Table.Cell colSpan={8} textAlign="center"> No User
                  Data</Table.Cell>
              </Table.Row>}
          </Table.Body>
        </Table>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    loading: state.app.loading,
  };
}

const mapDispatchToProps = {
  fetchFinish,
  fetchStart,
};

export default connect(mapStateToProps, mapDispatchToProps)(UserList);