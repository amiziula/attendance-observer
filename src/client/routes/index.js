import React from 'react';
import {Route, Switch, Redirect} from 'react-router-dom';

import MainPage from '../pages/main-page';
import FAQ from '../pages/faq';
import UserList from '../pages/user-list';
import StatisticList from '../pages/statistic-list';
// import UserSettings from '../pages/user-settings';
import UserAdd from '../pages/user-add';
import SubjectList from '../pages/subject-list';
import SubjectAdd from '../pages/subject-add';

import LoginPage from '../pages/login';
import LogoutPage from '../pages/logout';

export function Routes() {
  return (
    <Switch>
        <Route exact component={MainPage} path='/' />
        <Route component={FAQ} path='/faq' />
        <Route exact component={UserAdd} path='/user/:id?' />
        <Route exact component={UserList} path='/users/' />
        <Route component={StatisticList} path='/statistic/' />
        <Route component={SubjectList} path='/subjects/' />
        <Route exact component={SubjectAdd} path='/subject/:id?' />
        <Route component={LogoutPage} path='/logout'/>
      <Redirect to="/"/>
    </Switch>
  );
}

export function AuthRoutes() {
  return (
    <Switch>
      {/*<Route component={register} path='/register'/>*/}
      <Route component={LoginPage} path='/login'/>
      <Redirect to="/login"/>
    </Switch>
  );
}