import React, {Component} from 'react';
import {HashRouter} from 'react-router-dom';
import {connect} from 'react-redux';

import MetaTags from 'react-meta-tags';

import {Routes, AuthRoutes} from './routes';

import Header from './common/Header';
import ProgressLine from './common/ProgressLine';

import './assets/scss/main.scss';

class App extends Component {
  render() {
    const isAuthenticated = !!this.props.token;
    return (
      <HashRouter>
        <div className="shell">
          <MetaTags>
            <title>Observer</title>
            <meta name="viewport"
                  content="width=device-width, initial-scale=1"/>
          </MetaTags>

          <ProgressLine/>

          {isAuthenticated ? (
            <React.Fragment>
              <Header/>
              <div className="content">
                <Routes/>
              </div>
            </React.Fragment>
          ) : (
            <AuthRoutes/>
          )}

        </div>
      </HashRouter>
    );
  }
}

export default connect(state => {
  return {
    token: state.auth.token,
  };
})(App);