module.exports = function(api) {
  api.cache(false);
  return {
    presets: [
      '@babel/preset-env',
      '@babel/preset-react',
    ],
    plugins: [
      [
        'module-resolver',
        {
          cwd: 'babelrc',
          alias: {
            '@': './src',
          },
        },
      ],
      [
        '@babel/plugin-proposal-class-properties',
      ],
      [
        '@babel/plugin-proposal-export-default-from',
      ],
    ],
  };
};

// it needs for eslint alias resolver
if (this.System !== undefined) {
  System.config({
    paths: {
      '@/*': './src/*',
    },
  });
}
